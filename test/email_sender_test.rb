require 'minitest/autorun'
require 'email_sender'

describe EmailSender do
  class FakeEmail
    attr_reader :mail

    def initialize(*args, &block)
      @mail = Mail::Message.new(args, &block)
    end

    def deliver!
      # do nothing, cause we don't want real emails send out here
    end
  end

  before do
    Mail.stub(:new, FakeEmail.new)
  end

  describe "when everything works" do
    it "sends out the expected emails" do
      skip
    end
  end
end

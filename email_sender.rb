class EmailSender
  VAR_TEMAPLATE = /\{\{(.*?)\}\}/

  def initialize(smtp_args: {})
    smtp_creds = build_smtp_creds(smtp_args)

    Mail.defaults do
      delivery_method(:smtp, smtp_creds)
    end
  end

  def send_email_with_template(template, data: {}, config: { require_vars: true })
    raise ArgumentError.new('a template is required') if template.nil?
    raise ArgumentError.new('data is required') unless is_valid_data?(data)

    if config.fetch('require_vars') && !template.match(VAR_TEMAPLATE).nil?
      raise ArgumentError.new('the template does not have proper syntax for variable inject, please use: {{ var }}')
    end

    email = build_email_for_vars(template, data)

    begin
      email.deliver!
    rescue Exception => e
      puts "email was not delivered, error: #{e.message}, email structure: #{email.inspect}"
    end
  end

  private

  def is_valid_data?(data)
    required_keys = []
  end

  def build_email_for_vars(template, data)
    Mail.new do
      from     data.fetch('from')
      to       data.fetch('to')
      subject  data.fetch('subject')
      body     transformed_template(template, data)
    end
  end

  def transformed_template(template, data)
    new_template = template.dup

    data.each do |key, value|
      new_template.gsub!("{{ #{data} }}", value)
    end

    new_template
  end

  def build_smtp_creds(args: {})
    required_keys = ['address', 'port', 'user_name', 'password', 'authentication']
    allowed_keys = required_keys.dup.concat('domain')

    # raise an error if not all of the required keys are included
    if required_keys.all?{ |k| args.keys.include?(k) }
      raise ArgumentError.new("All of the following keys are required for the smtp hash: #{required_keys.join(', ')}")
    end

    args.dup.keep_if{ |k, _| allowed_keys.include?(k) }
  end
end

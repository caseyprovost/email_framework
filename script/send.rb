#!/usr/bin/env ruby

require 'rubygems'
require 'mail'
# require 'minitest' # uncomment if you wnat to test
require 'yaml'
require 'json'

base_folder = File.expand_path('../../', __FILE__)
Dir["#{base_folder}/*.rb"].each{ |f| require f }

## TEMPLATE FILE EXAMPLE
## <h2>Hello {{ username }},</h2><div>We are doing something cool</div>

## DATA FILE EXAMPLE
## [
##   { "username": "joe public" }
## ]

# Takes a template and array of hashes to send out emails.
# Example Usage
# Arg0 = the text file to be used as the email body
#   * This file should have variables defined using  {{ var }} syntax
# Arg1 = text ruby file that contains the array of hashes of vars to use to send the email
# ruby ./script/send.rb '/Users/you/templates/mytemplate.txt /Users/you/template_data/data.rb'

TEMPLATE_PATH = ARGV[0]
DATA_FILE_PATH = ARGV[1]

if TEMPLATE_PATH.nil? || TEMPLATE_PATH == ''
  raise ArgumentError.new('template path must be defined')
end

if DATA_FILE_PATH.nil? || DATA_FILE_PATH == '' || !File.exists?(DATA_FILE_PATH)
  raise ArgumentError.new('data_file must be defined')
end

# These need to be changed to your creds for the proper smtp service
smtp_arguments = YAML.load_file(File.join('config', 'smtp.yml'))

# This is the template to be used as the email body
template = File.read(TEMPLATE_PATH)

# This is the data file used to inject vars into the template
data = JSON.parse(File.read(DATA_FILE_PATH))

raise ArgumentError.new('data is required') if data.count == 0
raise ArgumentError.new('data must be an array of hashes') if data.count > 0 && !data[0].is_a?(Hash)

email_sender = EmailSender.new(smtp_arguments)

data.each do |email_data|
  email_sender.send_email_with_template(template, email_data)
end
